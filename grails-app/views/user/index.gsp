<%--
  Created by IntelliJ IDEA.
  User: janrudovsky
  Date: 22.07.12
  Time: 21:05
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title></title>
</head>
<body>
<g:form controller="user" action="createUser" id="${user?.id}">
    <g:textField name="name" value="${user?.name}"></g:textField>
    <g:submitButton name="submit">Odeslat</g:submitButton>
</g:form>

<g:each in="${users}" var="u">
    <g:link action="index" id="${u.id}">${u.name}</g:link> &nbsp;
    <g:link action="deleteUser" id="${u.id}">[delete]</g:link>
    <br>
</g:each>
</body>
</html>