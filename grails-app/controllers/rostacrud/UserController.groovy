package rostacrud

class UserController {

    def index() {

        User user
        if (params.id){
            user = User.findById(params.id)
        }
        List<User> users = User.findAll()
        render(view: 'index', model: [user: user, users: users])
    }

    def createUser(){
        // Tu by melo byt volani service
        User user
        if (!params.id){
            user = new User(name: params.name).save(failOnError: true)
        }else{
            user = User.findById(params.id)
            user.name = params.name
            user.save(failOnError: true)
        }
        redirect(view: 'index', model: [user: user])
    }

    def deleteUser(){
        User user = User.findById(params.id)
        user.delete(flush: true)
        redirect(action: 'index')
    }

}
